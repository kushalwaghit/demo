
const chars = "#%ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@1234567890";
let result = '';

function Generate() {

    // let result = ' ';
    for (let i = 0; i < 12; i++) {
        result += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    document.querySelector(".para").value = result;
}

let btn = document.querySelector(".copyBtn");

function copy() {
    if (result != '') {
        btn.innerHTML = "copied";
    }
    setTimeout(() => {
        btn.innerHTML = "copy";
    }, 1100);
    let copyText = document.querySelector(".btns");

    if (navigator && navigator.clipboard && navigator.clipboard.writeText)
        return navigator.clipboard.writeText(copyText.value);
    return Promise.reject('The Clipboard API is not available.');


}


